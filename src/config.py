import os

# Base Configuration for use of COnfig
use_http = 'https'

basedir = os.path.abspath(os.path.dirname(__file__))
port = 5000
tbl_file_process_name = 'Presidency_Data.tbl_files_process'

# Service for Upload File
host_upload_file = '192.168.1.17'
port_upload_file = '8006'

# Service for Login Service
host_login = '192.168.1.17'
port_login = '8501'

# Servicer for Crud Service
host_crud = '192.168.1.17'
port_crud = '8500'

# SQL Data
driver = 'ODBC Driver 17 for SQL Server'
server = '192.168.1.11'
user = 'ITAS'
password = 'Crisalida'
instance = 'SQLEXPRESS'
database = 'DB_Presidency_Robotec'
