import config, requests

from datetime import datetime

from json import JSONEncoder, dumps, loads

from db.connection import create_connection
from db.dao.dao_mssql import create, read

from flask import request, jsonify
from flask_restful import Resource, abort

class CustomEncoder(JSONEncoder):
    def default(self, obj):
        if set(['quantize', 'year']).intersection(dir(obj)):
            return str(obj)
        elif hasattr(obj, 'next'):
            return list(obj)
        return JSONEncoder.default(self, obj)

def is_token_valid(auth):
    if not (auth):
        return False, {}

    response_validation = requests.post('https://{}:{}/login/validate'.format(
        config.host_login,
        config.port_login
    ), headers={'Authorization': auth}, verify=False)

    if (response_validation.status_code == 401):
        return False, {}
    else:
        data_response = response_validation.json()
        return True, data_response


class File_process(Resource):
    tables = config.tbl_file_process_name

    def get(self):
        if not (request.headers.has_key('authorization') or
                request.headers.has_key('Authorization')):
            return {
                'code': 401,
                'message': 'Unauthorized',
                'description': 'Token not Found.'
            }

        is_auth, _ = is_token_valid(request.headers['authorization'])
        if not (is_auth):
            return {
                'code': 401,
                'message': 'Unauthorized',
                'description': 'Authorized Token Not Valid.'
            }, 401
        attributes = '*'

        data = read(attributes, self.tables)
        return jsonify({
            'code': 200,
            'count': len(data),
            'rows': data,
            'message': 'Done Correctly.'
        })


    def post(self):
        # Get Data

        if not (request.headers.has_key('authorization') or
                request.headers.has_key('Authorization')):
            return {
                'code': 401,
                'message': 'Unauthorized',
                'description': 'Token not Found.'
            }

        try:
            data = request.get_json(force=True)
        except:
            return { 'code': 400,
                     'message': 'Body is Clear',
                     'error': 'Data Not Found' }, 400

        is_auth, auth = is_token_valid(request.headers['authorization'])
        if not (is_auth):
            return {
                'code': 401,
                'message': 'Unauthorized',
                'description': 'Authorized Token Not Valid.'
            }, 401
        use_id = auth['user_id']

        keys = list(data.keys())
        values = list(data.values())

        # Verify data
        if not ('fproc_url_data' in data or
                'fproc_url_service_template' in data or
                'fproc_state' in data):
            return {
                       'code': 400,
                       'message': 'data is not valid, it have to fproc_url_data, '
                                  'fproc_url_service_template, fproc_state, use_id '
                   }, \
                   400

        # Put Audit and is_valid = true
        keys.append('fproc_is_valid')
        values.append(1)
        keys.append('fproc_created_at')
        values.append(datetime.now())
        keys.append('fproc_updated_at')
        values.append(datetime.now())

        print("Id de Usuario - {}".format(use_id))
        keys.append('use_id')
        values.append(use_id)
        keys.append('fproc_creator_id')
        values.append(use_id)
        keys.append('fproc_updater_id')
        values.append(use_id)

        message, code = {}, 200
        data_created = create(self.tables, keys, values)
        data_process = dumps(data_created, cls=CustomEncoder)
        if (len(data_created) >= 1):
            message, code = {
                'code': 201,
                'message': 'model created correctly',
                'data': loads(data_process)
            }, 201
        else:
            message, code = {'code': 401, 'error': 'model is not created'}, 401

        return message, code
