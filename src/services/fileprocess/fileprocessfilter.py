import config, json, requests

from flask import request, jsonify
from flask_restful import Resource, abort

def is_token_valid(auth):
    if not (auth):
        return False, {}

    response_validation = requests.post('https://{}:{}/login/validate'.format(
        config.host_login,
        config.port_login
    ), headers={'Authorization': auth}, verify=False)

    if (response_validation.status_code == 401):
        return False, {}
    else:
        data_response = response_validation.json()
        return True, data_response


class File_process_filter(Resource):
    tables = config.tbl_file_process_name

    def post(self):
        # Verify in Headers authorization
        if not (request.headers.has_key('authorization') or
                request.headers.has_key('Authorization')):
            return {
                'code': 401,
                'message': 'Unauthorized',
                'description': 'Token not Found.'
            }

        is_auth, auth = is_token_valid(request.headers['authorization'])

        # Verify if it's valid
        if not (is_auth):
            return {
                'code': 401,
                'message': 'Unauthorized',
                'description': 'Authorized Token Not Valid.'
            }, 401
        use_id = auth['user_id']

        try:
            data = request.get_json(force=True)
        except:
            return { 'code': 400,
                     'message': 'Body is Clear',
                     'error': 'Data Not Found' }, 400

        if not ('filter' in data):
            return { 'code': 400,
                     'message': 'The Filter Property is Required',
                     'error': 'Not complete data' }, 400

        data['log'] = {
            'action': 'Filter',
            'originAgent': request.headers['user-agent'],
            'originIp': request.remote_addr,
            'targetElement': self.tables,
            'user_id': use_id
        }

        data['joins'] = [ 'tbl_user' ]

        # Crud-Response
        response_crud = requests.post('https://{}:{}/crud/{}'.format(
            config.host_crud,
            config.port_crud,
            self.tables.split(".")[1]
        ), json=data, verify=False)

        return response_crud.json()
