import config, requests

from json import JSONEncoder, dumps, loads
from datetime import datetime

from db.connection import create_connection
from flask import request, jsonify
from flask_restful import Resource, abort
from db.dao.dao_mssql import read, update

class CustomEncoder(JSONEncoder):
    def default(self, obj):
        if set(['quantize', 'year']).intersection(dir(obj)):
            return str(obj)
        elif hasattr(obj, 'next'):
            return list(obj)
        return JSONEncoder.default(self, obj)


def is_token_valid(auth):
    if not (auth):
        return False, {}

    response_validation = requests.post('https://{}:{}/login/validate'.format(
        config.host_login,
        config.port_login
    ), headers={'Authorization': auth}, verify=False)

    if (response_validation.status_code == 401):
        return False, {}
    else:
        data_response = response_validation.json()
        return True, data_response


class File_process_id(Resource):
    tables = config.tbl_file_process_name

    def get(self, id_file):
        if not (request.headers.has_key('authorization') or
                request.headers.has_key('Authorization')):
            return {
                'code': 401,
                'message': 'Unauthorized',
                'description': 'Token not Found.'
            }

        is_auth, _ = is_token_valid(request.headers['authorization'])
        if not (is_auth):
            return {
                'code': 401,
                'message': 'Unauthorized',
                'description': 'Authorized Token Not Valid.'
            }, 401

        attributes = '*'
        condition = 'fproc_id = {}'.format(id_file)

        data = dumps(read(attributes, self.tables, condition), cls=CustomEncoder)
        print("data - {}".format(data))
        return {
            'message': 'Done Correctly.',
            'code': 200,
            'rows': loads(data)
        }


    def patch(self, id_file):
        # Get Data

        if not (request.headers.has_key('authorization') or
                request.headers.has_key('Authorization')):
            return {
                'code': 401,
                'message': 'Unauthorized',
                'description': 'Token not Found.'
            }

        is_auth, auth = is_token_valid(request.headers['authorization'])
        if not (is_auth):
            return {
                'code': 401,
                'message': 'Unauthorized',
                'description': 'Authorized Token Not Valid.'
            }, 401
        use_id = auth['user_id']

        try:
            data = request.get_json(force=True)
        except:
            return { 'code': 400,
                     'message': 'Body is Clear',
                     'error': 'Data Not Found' }, 400

        if not data:
            return {'code': 401, 'error': 'Data not Send'}, 401

        # Verify data
        if ('fproc_created_at' in data or
            'fproc_updated_at' in data or
            'fproc_deleted_at' in data or
            'fproc_creator_id' in data or
            'fproc_updater_id' in data or
            'fproc_deleter_id' in data or
            'fproc_id' in data or
            'fproc_is_valid' in data):
            return {
                'code': 400,
                'message': 'data is not updated'
            }, \
            400

        message, code = {}, 200
        condition = 'fproc_id = {}'.format(id_file)

        keys = list(data.keys())
        values = list(data.values())

        # Put Audit and is_valid = true
        keys.append('fproc_updated_at')
        values.append(datetime.now())

        print("Id de Usuario - {}".format(use_id))
        keys.append('fproc_updater_id')
        values.append(use_id)

        if update(self.tables, keys, values, condition) == 1:
            message, code = {'code': 201, 'message': 'model updated correctly'}, 201
        else:
            message, code = {'code': 401, 'error': 'model is not updated'}, 401

        return message, code


    def delete(self, id_file):
        # Delete user

        if not (request.headers.has_key('authorization') or
                request.headers.has_key('Authorization')):
            return {
                'code': 401,
                'message': 'Unauthorized',
                'description': 'Token not Found.'
            }

        is_auth, auth = is_token_valid(request.headers['authorization'])
        if not (is_auth):
            return {
                'code': 401,
                'message': 'Unauthorized',
                'description': 'Authorized Token Not Valid.'
            }, 401
        use_id = auth['user_id']

        message, code = {}, 200
        condition = 'fproc_id = {}'.format(id_file)

        keys = ['fproc_is_valid', 'fproc_deleted_at', 'fproc_deleter_id']
        values = [0, datetime.now(), use_id]

        if update(self.tables, keys, values, condition) == 1:
            message, code = {'code': 201, 'message': 'deleted correctly'}, 201
        else:
            message, code = {'code': 401, 'error': 'register is not updated'}, 401

        return message, code
