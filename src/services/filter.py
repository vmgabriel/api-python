import config
import pyodbc
from db.connection import create_connection


def get_and_change_attribute_relation(attribute_relation, data):
    for relation in attribute_relation:
        # Get Attribute Relation
        relation_data = relation['relation']
        attribute_to_change = relation['attribute']
        type_data = relation['type']

        # query
        query = 'SELECT {} FROM presidency_data.{} WHERE {} {}'.format(
            relation_data['get'],
            relation_data['table'],
            relation_data['attribute'],
            relation_data['op']
        )
        if type_data == 'string' or type_data == 'date':
            query += '\'{}\';'.format(
                data[attribute_to_change]
            )
        else:
            query += '{};'.format(
                data[attribute_to_change]
            )

        # Connection with database
        cursor = create_connection().cursor()

        # Execute sql
        cursor.execute(query)

        data[attribute_to_change] = cursor.fetchall()[0][0]

    return data
