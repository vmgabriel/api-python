from flask import Blueprint
from flask_restful import Api

# Resources
from resources.base.index import Index
from resources.base.about import About
from resources.process.xlsx import Xlsx
from services.fileprocess.fileprocesslist import File_process
from services.fileprocess.__index__ import File_process_id
from services.fileprocess.fileprocessfilter import File_process_filter

api_bp = Blueprint('api', __name__)
api = Api(api_bp)

# Route
api.add_resource(Index, '/')
api.add_resource(About, '/about')

api.add_resource(File_process, '/file-process')
api.add_resource(File_process_filter, '/file-process/filter')
api.add_resource(File_process_id, '/file-process/<int:id_file>')

api.add_resource(Xlsx, '/processing')
