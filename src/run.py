import requests, config, sys
from flask import Flask


def create_app(config_filename):
    tables = config.tbl_file_process_name

    # build Model in Crud-Service

    data = {'table': tables.split('.')[1]}

    response_crud = requests.post('https://{}:{}/crud/create-model'.format(
        config.host_crud,
        config.port_crud
    ), json=data, verify=False)

    if (response_crud.status_code != 200):
        print ("------------------------")
        print ("A Error Occurred while Build Model of Crud Service - {}".format(
            tables.split('.')[1])
        )
        print ("------------------------")
        sys.exit("A Error Occurred while Build Model of Crud Service - {}".format(
            tables.split('.')[1])
        )
    else:
        print ("Model - {} - Build Correctly.".format(tables.split('.')[1]))

    app = Flask(__name__)
    app.config.from_object(config_filename)

    from app import api_bp
    app.register_blueprint(api_bp, url_prefix='/api')

    # from Model import db
    # db.init_app(app)

    return app


if __name__ == "__main__":
    app = create_app("config")
    app.run(debug=True)
