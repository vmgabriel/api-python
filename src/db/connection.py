import pyodbc
import config
from db.sqlserverport import lookup


instance = ''
if bool(config.instance):
    instance = '{},{}'.format(config.server, lookup(config.server, config.instance))
else:
    instance = config.server


def create_connection():
    conn_string = 'Trusted_Connection=no;DRIVER={' + config.driver + '};'
    conn_string += 'SERVER={};DATABASE={};UID={};PWD={}'.format(
        instance,
        config.database,
        config.user,
        config.password
    )
    print('datos {}'.format(conn_string))
    conn = pyodbc.connect(conn_string)

    return conn
