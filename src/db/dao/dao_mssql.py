import pyodbc, functools
from db.connection import create_connection

data_values = {}

def convert_to_str(input_seq, seperator):
    return seperator.join(input_seq)


def line(data):
    if (len(data) == 1):
        return '?'
    return '{},?'.format(line(data[:-1]))


def line_to_line(values):
    array_converted = list(map(lambda x: '({})'.format(line(x)), values))
    return functools.reduce(lambda x,y: '{},{}'.format(x,y), array_converted)


def equal_value(data):
    return map(
        lambda i: '{} = ?'.format(i),
        data
    )


def send_query(query, params=[]):
    # Connection with database
    cnxn = create_connection()
    cursor = cnxn.cursor()

    # Execute sql
    if not params:
        print("No tiene parametros")
        cursor.execute(query)
        return cursor
    else:
        print("Tiene Parametros")
        cursor.execute(query, params)
        data_values = cursor.fetchone()
        cnxn.commit()
        return cursor, data_values


def create(table, values, data, return_inserted=True):

    query = 'INSERT INTO {} ({}) '.format(
        table,
        convert_to_str(values, ','),
    )

    if (return_inserted):
        query += 'OUTPUT INSERTED.* '

    query += 'VALUES {};'.format('({})'.format(line(data)))

    print(query)

    results = []

    cur, data_sql = send_query(query, data)
    columns = [column[0] for column in cur.description]
    results.append(dict(zip(columns, data_sql)))

    return results


def read(attributes, tables, conditions='', joins='', order=''):
    query = 'SELECT {} from {}'.format(attributes, tables)

    if bool(joins):
        query += ''

    if bool(conditions):
        query += ' WHERE {} '.format(conditions)

    if bool(order):
        query += ' ORDER BY {}'.format(order)

    print(query)

    results = []

    data_sql = send_query(query)
    columns = [column[0] for column in data_sql.description]

    for row in data_sql.fetchall():
        results.append(dict(zip(columns, row)))

    return results


def update(table, keys, values, conditions):
    query = 'UPDATE {} SET {} WHERE {};'.format(
        table,
        convert_to_str(equal_value(keys), ','),
        conditions
    )
    print(query)
    cur, data_sql = send_query(query, values)
    return cur.rowcount


def delete(tables, conditions):
    query = 'DELETE FROM {} WHERE {};'.format(tables, conditions)

    cur, data_sql = send_query(query)
    return cur.rowcount
