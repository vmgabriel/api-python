from flask_restful import Resource


class About(Resource):
    def get(self):
        return {"message": "Robotec Colombia S.A.S"}
