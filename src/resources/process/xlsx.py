from flask import request, jsonify
import ssl, wget, config, re, os, datetime, threading, functools, requests

from flask_restful import Resource, abort
from openpyxl import load_workbook

from services.filter import get_and_change_attribute_relation


def set_interval(func, sec):
    def func_wrapper():
        set_interval(func, sec)
        func()
    t = threading.Timer(sec, func_wrapper)
    t.start()
    return t


def convert_attribute(data, type_data):
    if type_data == 'string':
        return '{}'.format(data), 'ok'
    elif type_data == 'int':
        return int(data), 'ok'
    elif type_data == 'date':
        return datetime.strptime(data), 'ok'
    elif type_data == 'email':
        email_re = re.search('(\w+[.|\w])*@(\w+[.])*(com$|in$)',data)
        if bool(email_re):
            return '{}'.format(data), 'ok'
        else:
            return 'email is not well written', 'error'
    else:
        return data, 'nf'


def convert_attributes(row_data, model_data):
    output_conversion = dict()
    state = 'ok'
    message = ''

    for attribute in row_data:
        model_temp = list(filter(lambda x: x['attribute'] == attribute, model_data))
        type_data = model_temp[0]['type']

        if 'relation' in model_temp[0]:
            type_data = model_temp[0]['relation']['type']

        convertion, output = convert_attribute(row_data[attribute], type_data)
        if output == 'error':
            state = output
            message += '{} convertion to {} -> error: {}'.format(row_data[attribute],
                                                               type_data,
                                                               convertion)
        elif output == 'nf':
            state = 'error'
            message += '{} not found type in {} - '.format(type_data, convertion)
        else:
            output_conversion = convertion

    return output_conversion, state, message


# Functional Function for Verify Format
def verify_format(ws, format_doc, col=1, row=1):
    if len(format_doc) == 1:
        return ws.cell(column=col, row=row).value == format_doc[0]
    return (
        ws.cell(column=col, row=row).value == format_doc[0] and
        verify_format(ws, format_doc[1:], col + 1, row)
    )


# get Data Valid of File
def get_data(ws, model, col=1, row=2):
    table = dict()

    first_cell_value = ws.cell(column=col, row=row).value
    while first_cell_value is not None:
        if not ws.row_dimensions[row].hidden:
            table[row] = dict()
            for col_data in range(col, len(model)):
                table[row][model[col_data - col]['attribute']] = ws.cell(column=col_data,
                                                                         row=row).value

        row += 1
        first_cell_value = ws.cell(column=col, row=row).value

    return table


# Delete File
def delete_file(route):
    route_file = 'temp/{}'.format(route)
    os.remove(route_file)


def verify_is_relation_valid(relation):
    return (
        'table' in relation['relation'] and
        'attribute' in relation['relation'] and
        'get' in relation['relation'] and
        'type' in relation['relation'] and
        'op' in relation['relation']
    )


def is_token_valid(auth):
    if not (auth):
        return False, {}

    response_validation = requests.post('https://{}:{}/login/validate'.format(
        config.host_login,
        config.port_login
    ), headers={'Authorization': auth}, verify=False)

    if (response_validation.status_code == 401):
        return False, {}
    else:
        data_response = response_validation.json()
        return True, data_response


class Xlsx(Resource):
    # Type file valid

    file_types = ['.csv', '.xls', '.xlsx']
    data = dict()

    def post(self):
        if not (request.headers.has_key('authorization') or
                request.headers.has_key('Authorization')):
            return {
                'code': 401,
                'message': 'Unauthorized',
                'description': 'Token not Found.'
            }, 401

        is_auth, _ = is_token_valid(request.headers['authorization'])
        if not (is_auth):
            return {
                'code': 401,
                'message': 'Unauthorized',
                'description': 'Authorized Token Not Valid.'
            }, 401

        self.data = request.get_json(force = True)

        # Verify data in request
        if not ('url' in self.data or 'table' in self.data or 'model' in self.data):
            return {'code': 400, 'error': 'url, table or model is not defined'}, 400

        data_process = {
            'fproc_url_data': self.data['url'],
            'fproc_url_service_template': self.data['template'],
            'fproc_progress': 0,
            'fproc_state': 'REGISTERED'
        }

        request_file_process = requests.post(
            'http://localhost:{}/api/file-process'.format(config.port),
            json=data_process,
            headers=request.headers,
            verify=False
        )

        if (request_file_process.status_code == 201):
            self.process_file = request_file_process.json()['data'][0]

            set_interval(self.pred, 3)
            return {'message': "Done Correctly.", "code": 200}, 200
        else:
            return {'code': 400, 'message': 'Error to Initialize Report'}, 400


    def pred(self):
        # Obtein data of request
        # data = request.get_json(force = True)
        # Force https
        ssl._create_default_https_context = ssl._create_unverified_context

        print("-----------------------")
        print("process File - {}".format(self.process_file))
        print("-----------------------")

        # get Data
        url_data = self.data['url']
        model_data = self.data['model']
        table_data = self.data['table']
        relation_data = self.data['relation']

        print("Model - {}".format(model_data))
        print("Url - {}".format(url_data))
        print("Table - {}".format(table_data))
        print("Relation - {}".format(relation_data))

        # Verify data is not clear
        if not model_data:
            return {'code': 400, 'error': 'model is not have data'}, 400
        if not table_data:
            return {'code': 400, 'error': 'table is empty'}, 400

        # Call verify extension
        verify_extension = functools.reduce(lambda a, b:
                                            a or (len(re.findall(b, url_data))  > 0),
                                            self.file_types,
                                            True
        )
        print("verify_extension - {}".format(verify_extension))

        if not verify_extension:
            return {'code': 400, 'error':
                'format incorrect, the format valid is {}'.format(self.file_types)
                    }, 400

        # Create URL for download file
        url = '{}://{}:{}/{}'.format(config.use_http, config.host_upload_file,
                                     config.port_upload_file, url_data)
        print('Url - {}'.format(url))

        # Download file
        try:
            wget.download(url, 'temp/{}'.format(url_data))
        except:
            print('Error: A except ocurred when download file')

        # Errors in download file
        if not (os.path.isfile('temp/{}'.format(url_data))):
            error = 'resource has not been obtained correctly, you are sure that'
            error += ' the route is correct?'

            return {'code': 400,
                    'error': error}, 400

        # Read The Document
        data_file = load_workbook('temp/{}'.format(url_data))
        ws = data_file.worksheets[0]

        # Verify data of Row and Column
        row = 1
        if 'row' in self.data:
            row = self.data['row']
        col = 1
        if 'col' in self.data:
            col = self.data['col']

        print()
        print("---")
        print('col - {}'.format(col))
        print('row - {}'.format(row))

        # Obtein format and verify that is not empty
        format_data = list(map(lambda x: x['name'], model_data))
        print("- format_data - {}".format(format_data))

        if not format_data:
            delete_file(url_data)
            return {'code': 400, 'error': 'model not have a data name'}, 400

        # Call Verify Format
        print("verify_format(ws, format_data, col, row) - {}".format(
            verify_format(ws, format_data, col, row))
        )
        if not (verify_format(ws, format_data, col, row)):
            delete_file(url_data)
            return {'code': 400, 'error': 'File has not format defined'}, 400

        # Get Data
        table = get_data(ws, model_data, col, row + 1)
        if not (bool(table)):
            delete_file(url_data)
            return {'code': 400, 'error': 'Data not found in document'}, 400

        # Verify attributes of relation
        attribute_relation = list(filter(lambda x: bool(x.get('relation')), model_data))
        is_relation_valid = functools.reduce(verify_is_relation_valid, attribute_relation)

        if not (is_relation_valid and attribute_relation):
            delete_file(url_data)
            return {'code': 400,
                    'error': 'relation attribute have to table, attribute, get, type and op'}, 400

        # Get and Change Data, too convert type
        table_error = []
        if len(attribute_relation) > 0:
            for data in range(row + 1, len(table) + row):
                data = get_and_change_attribute_relation(attribute_relation, table[data])
                conversion, state, message = convert_attributes(data, model_data)
                print()
                if state == 'ok':
                    data = conversion
                else:
                    conversion['error'] = message
                    print(conversion)
                    table_error.append(conversion)

        delete_file(url_data)
        print("jsonify(table) - {}".format(jsonify(table)))
        # return jsonify(table)
